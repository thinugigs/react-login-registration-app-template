import React from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import './Register.scss'

class Login extends React.Component {

    constructor(){
        super();
        this.state = {
            invalidClass: "message invalid-message-default",
            name: "",
            birthday: "",
            password: ""
        }
    }

    registerClick(){
        if(this.isValid()){
            axios.post(`https://demo1344629.mockable.io/register`)
                .then(res => {
                    this.props.history.push('/login')
                })
        }
        else{
            this.setState({
                invalidClass: 'message invalid-message-active'
            });
        }
    }

    updateName(evt){
        this.setState({
            name: evt.target.value
        });
    }
    updateBirthday(evt){
        this.setState({
            birthday: evt.target.value
        });
    }
    updatePassword(evt){
        this.setState({
            password: evt.target.value
        });
    }

    isValid(){
        if(this.state.name.trim() == ''){
            return false;
        }
        else if(this.state.birthday.trim() == ''){
            return false;
        }
        else if(this.state.password.trim() == ''){
            return false;
        }
        return true;
    }

    hideInvalid(){
        this.setState({
            invalidClass: 'message invalid-message-default'
        });
    }

    render() {
        return (
            <div className={'register-div'}>
                <div className="login-page">
                    <div className="form">
                        <div className="login-form">
                            <input type="text" placeholder='Enter name' onChange={this.updateName.bind(this)} onClick={this.hideInvalid.bind(this)}/>
                            <input type="date" placeholder='Enter birthday' onChange={this.updateBirthday.bind(this)} onClick={this.hideInvalid.bind(this)}/>
                            <input type="password" placeholder='Enter Password' onChange={this.updatePassword.bind(this)} onClick={this.hideInvalid.bind(this)}/>
                            <button onClick={this.registerClick.bind(this)}>Register</button>
                            <p className="message">Already have an account? <Link to="/login">Login</Link></p>
                            <p className={this.state.invalidClass}>Invalid</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    };
}

export default Login;
