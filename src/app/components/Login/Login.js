import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import './Login.scss'

class Login extends React.Component {

    constructor(){
        super();
        this.state = {
            invalidClass: "message invalid-message-default",
            username: "",
            password: ""
        }
    }

    loginClick(){
        if(this.isValid()){
            axios.get(`https://demo1344629.mockable.io/login`)
                .then(res => {
                    this.props.history.push('/')
                })
        }
        else{
            this.setState({
                invalidClass: 'message invalid-message-active'
            });
        }
    }

    updateUsername(evt){
        this.setState({
            username: evt.target.value
        });
    }

    updatePassword(evt){
        this.setState({
            password: evt.target.value
        });
    }

    isValid(){
        if(this.state.username.trim() == ''){
            return false;
        }
        else if(this.state.password.trim() == ''){
            return false;
        }
        return true;
    }

    hideInvalid(){
        this.setState({
            invalidClass: 'message invalid-message-default'
        });
    }

    render() {
        return (
            <div className={'login-div'}>
                <div className="login-page">
                    <div className="form">
                        <div className="login-form">
                            <input type="text" placeholder='Enter Username' onChange={this.updateUsername.bind(this)} onClick={this.hideInvalid.bind(this)}/>
                            <input type="password" placeholder='Enter Password' onChange={this.updatePassword.bind(this)} onClick={this.hideInvalid.bind(this)}/>
                            <button onClick={this.loginClick.bind(this)}>login</button>
                            <p className="message">Not registered? <Link to="/register">Create an account</Link></p>
                            <p className={this.state.invalidClass}>Invalid</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    };
}

export default Login;
